# Hauerpower - Tworzenie Szablonów
[[_TOC_]]

# Informacje ogólne
Dostępne są 3 szablony bazowe:
- [Bazowy Szablon HTML](https://gitlab.com/tlisek_hp/bazowy-szablon-html)
- [Bazowy Szablon Twig](https://gitlab.com/tlisek_hp/bazowy-szablon-twig)
- [Bazowy Szablon WP](https://gitlab.com/tlisek_hp/bazowy-szablon-wp)

Wszystkie te szablony są ze sobą kompatybilne - maja taką samą strukturę plików i gulpfile. Szablony oparte są o [Foundation](https://get.foundation/sites/docs/) z wyłączoną większością modułów w celu zmniejszenia rozmiaru końcowego szablonu.

Dwa pierwsze służą do stworzenia statycznej strony HTML na podstawie makiet XD.
Ostatni służy do tworzenia szablonów PHP do Wordpressa (na podstawie statycznych HTML lub bezpośrednio z makiet XD).

# Tworzenie szablonu krok po kroku

Zwykle proces tworzenia szablonu wygląda następująco:
Makieta XD > Szablon statyczny HTML > Szablon Docelowy WP

Wszystkie poprawki później są nanoszone już na szablonie docelowym w Wordpressie.

## Szablon statyczny (HTML)

### Przygotowanie do pracy
1. Zakładamy nowe repozytorium (prywatne)
2. W repozytorium konfigurujemy _Pages_ - wchodzimy w _Settings_ > _General_ > zakładka _Visibility, project features, permissions_ > na dole ustawiamy _Pages_ na "_Everyone_"
2. Klonujemy szablon bazowy (1. lub 2.) - polecam szablon Twig - mamy możliwość importowania plików (np. header lub footer), później łatwiej jest też  przenieść wszystko na PHP i podzielić szablon
3. Wgrywamy do nowego repozytorium

Gitlab automatycznie skompiluje projekt i opublikuje zawartość katalogu `dist` jako Pages.

### Rozpoczęcie kodowania
1. Uruchamiamy `npm install`
2. Sprawdzamy kompilację uruchamiając `gulp prod`

Zgodnie z `README.md` w każdym z projektów bazowych, oprócz Foundation, zainstalowane są także biblioteki:
- slick-carousel - do sliderów
- fancybox - z dodanym wsparciem dla natywnych galerii WP
- lazysizes - z dodanym wsparciem do obrazków dodawanych w WP

W celu dodania innych bibliotek, instalujemy je przy pomocy `npm install`. 
Wymagane pliki JS dodajemy w `gulpfile.js` do zmiennej `JSlibs` - zostaną skompilowane z innymi bibliotekami i załączone w pliku `libs.js`. **NIE dodajemy dodatkowych skryptów poprzez `<script>` w plikach HTML**

Jeśli biblioteka wymaga styli css to, ze względu na to że czasem wymagają dodatkowych plików (np. obrazków czy czcionek), dodajemy ścieżkę całego folderu do zmiennej `pluginsToCopy` (oczywiście, jeśli w folderze wtyczki w node_modules jest np. folder dist, to dodajemy jego ścieżkę, żeby nie kopiowało bez sensu też plików źródłowych). Pliki zostaną skopiowane do szablonu - do katalogu `/vendor`, a my możemy je załączyć w `<head>` przykładowo:
```html
<link rel="stylesheet" href="./vendor/slick-carousel/slick/slick.css">
```

### Browsersync
Projekt możemy skompilować jednorazowo komendą `gulp prod`, lub włączyć stałe wykrywanie zmian komendą `gulp dev`.

## Szablon docelowy (PHP Wordpress)

### Przygotowanie do pracy
1. Klonujemy repozytorium z szablonem bazowym WP
2. Uruchamiamy `gulp prod` (lub `gulp dev` dla [Browsersync](#browsersync))
 
### Lokalna instalacja WP
1. Na serwerze lokalnym (MAMP / XAMPP / WAMP) tworzymy nowy katalog na instalację wordpressa. Dla wygody, możemy także stworzyć dla projektu Virtual Host ([Instrukcja dla MAMP](https://dev.to/crankysparrow/configuring-virtual-hosts-with-mamp-f3i), [Instrukcja dla XAMPP](https://royalcode.eu/jak-skonfigurowac-virtual-host-w-xampp/)).

2. W PHPMyAdmin tworzymy nową bazę danych dla projektu.

3. Uruchamiamy nasz serwer lokalny i otwieramy go w przeglądarce. Instalujemy wordpressa.

### Linkowanie repozytorium z szablonem do Wordpressa

Musimy podłączyć katalog `/dist` z szablonu WP do folderu `/wp-content/themes`. W tym celu uruchamiamy komendę:

```bash
# DLA UNIX:
ln -s /path/to/szablon/dist /path/to/wp/wp-content/themes/theme-name
```
```bash
# DLA WINDOWS:
mklink /D "C:/path/to/wp/wp-content/themes/theme-name" "C:/path/to/szablon/dist"
```

### Tworzenie szablonu WP
1. W szablonie PHP ustawiamy w `config.json` adres URL lokalnej instalacji wordpressa (plik ten nie jest wgrywany na wordpressa, każdy może mieć własny adres loaclhost)
2. Pobieramy repozytorium z szablonem HTML
3. Przenosimy foldery `/src/assets`, `/src/js` i `/srs/scss` z szablonu HTML do folderu `/src` szablonu WP
4. Przenosimy konfigurację z `gulpfile.js` na jeden ze sposobów:
	1. Przekopiowanie całego `gulpfile.js` z szablonu HTML do WP - __konieczna zmiana zmiennej `is_wordpress` na `true`__
	2. Przekopiowanie tylko zmiennych `JSlibs` i `pluginsToCopy`
5. __Usuwamy ze zmiennej `JSlibs` jQuery__ - wordpress już zapewnia jQuery
6. W pliku `src/style.css` zmieniamy informacje o szablonie
7. W pliku `src/functions.php` zmieniamy `THEME_NS`
8. Na podstawie plików HTML tworzymy szablony w WP

### Wgrywanie na FTP
Po skończeniu pracy nad szablonem, gitlab automatycznie go skompiluje i wgra pliki na FTP. W tym celu trzeba skonfigurować repozytorium i szablon:
1. W repozytorium dla szablonu WP przechodzimy do _Settings_ > _CI/CD_ > zakładka _Variables_
2. Wprowadzamy wartości zmiennych
```bash
$FTP_USER # przykładowo wpstrony
$FTP_PASS # uwaga - jeśli w haśle występuje znak $, zapisujemy go jako $$
$FTP_HOST # przykładowo wpstrony.linuxpl.info
$FTP_DIR # przykładowo /domains/wpstrony.linuxpl.info/public_html/codibly/wp-content/themes/codibly-theme
```
3. W pliku `.gitlab-ci.yml` odkomentowjemy blok pod `Sync FTP Roboczy`
4. Wgrywamy zmiany do repozytorium

Wszystkie zmiany w szablonie muszą być robione przez gitlaba. 
__Nie edytujemy plików bezpośrednio na FTP, czy w _Edytorze motywu_ wordpressa - zostana nadpisane przy następnym commicie__

__Uwaga!__ Czasem (szczególnie przy niewielkich zmianach w pliku) gitlab nie przegra wszystkich plików na FTP. Jeśli jest podejrzenie, że coś jest nie tak, sprawdzamy wynik uruchomionego _Job_, gdzie widnieje lista przeniesionych plików. W takim przypadku można spróbować zrobić większą zmianę w pliku, lub ostatecznie przegrać te brakujące bezpośrednio na FTP.

Po zakończeniu pracy na roboczym, aby wgrać szablon na serwer docelowy, analogicznie do roboczego, ustawiamy zmienne
```bash
$FTP_USER_PROD
$FTP_PASS_PROD
$FTP_HOST_PROD
$FTP_DIR_PROD
```
i odkomentowujemy odpowiedni blok w `.gitlab-ci.yml`

# Praktyki kodowania
## Ogólne (dla szablonu statycznego)

### Konfiguracja Foundation
Na samym początku tworzenia projektu, w pliku `scss/modules/_foundation-settings.scss` warto ustawić sobie na start zmienne takie jak szerokość containera (domyślnie 1130px), szerokość marginesów, czy w szczególności kolory (głownie `$primary-color`). Przyspieszy to znacznie start, dając gotowe kolory linków, przycisków i szerokości elementów.

Jeśli potrzebujemy włączyć dodatkowe moduły foundation, musimy je odkomentować w pliku `scss/modules/_foundation-styles.scss`. Jeśli wymagają bibliotek JS, dodajemy stosowne w `gulpfile.js` zgodnie z sekcją [Rozpoczęcie kodowania](#rozpoczęcie-kodowania).

### Układ strony

Do tworzenia głównego układu strony używamy elementów z _Foundation Grid_.

Do ustalenia szerokości kontenera:
```html
<div class="grid-container">
	<p>Content</p>
</div>
```
Do ustalenia grida:
```html
<div class="grid-x grid-margin-x grid-margin-y">
	<div class="small-12 meium-8 cell"></div>
	<div class="small-12 meium-4 cell"></div>
</div>
```

Bloki `.cell` zawsze muszą być bezpośrednio w blokach `.grid-x`. Nie mieszamy także klas, czyli:

```html
<!-- TAK JEST DOBRZE -->
<div class="grid-x grid-margin-x grid-margin-y">
	<div class="small-12 medium-8 cell">
	
		<div class="grid-x grid-margin-x grid-margin-y">
			<div class="small-12 meium-6 cell"></div>
			<div class="small-12 meium-6 cell"></div>
		</div>
		
	</div>
	<div class="small-12 meium-4 cell"></div>
</div>
```

```html
<!-- TAK JEST ŹLE -->
<div class="grid-x grid-margin-x grid-margin-y">
	<div class="small-12 medium-8 cell grid-x grid-margin-x grid-margin-y">
		<div class="small-12 meium-6 cell"></div>
		<div class="small-12 meium-6 cell"></div>
	</div>
	<div class="small-12 meium-4 cell"></div>
</div>
```

Takie działania zapewnią, że wszystkie elementy na stronie będą równo ze sobą z prawej i lewej strony, bez żadnych niechcianych odstępów 15px.

Generalnie bloki `.grid` i `.cell` stosujemy tylko to tworzenia układów i nie stylujemy, jeśli chcemy coś w nich umieścić, zagnieżdżamy, np.

```html
<section class="section section--news">
	<div class="grid-container">
		<h2 class="section__title">Aktualności</h2>
		<div class="news">
			<div class="grid-x grid-margin-x grid-margin-y">
				<div class="small-12 medium-4 large-3 cell">
				
					<!-- .news__tile jest zagnieżdzony w .cell -->
					<a class="news__tile" href="#">
						<h3 class="news__title">News</h3>
					</a>
					
				</div>
			</div>
		</div>
	</div>
</section>
```

### JavaScript
Pliki JS tworzymy w osobnych modułach w katalogu `/src/js/modules`. Przykładowo wszystkie skrypty dotyczące sliderów są w pliku `src/js/modules/sliders.js`. Struktura modułów wygląda następująco:
```javascript
export  default function slidersInit(){
    (function($){
        // Kod
    })(jQuery);
}
```
Uwaga! Blok `(function($){  })(jQuery);` jest ważny ze względu na kompatybilność z jQuery w Wordpressie.

Następnie w pliku głównym `src/js/app.js` dodajemy odpowiednio:
``` javascript
import slidersInit from './modules/sliders';
```
oraz 
```javascript
slidersInit();
```

### BEM (HTML + CSS)
CSS staramy się pisać w konwencji [BEM](https://www.nafrontendzie.pl/metodyki-css-2-bem). Pliki scss dzielimy tematycznie (najlepiej - każdy blok w osobnym pliku). Przykładowo tworząc w szablonie sekcję CTA mamy:

```html
<!-- Przykładowy plik /partials/cta.twig -->
<section class="cta">
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y align-middle">
            <div class="small-12 medium-6 cell">
                <h3 class="cta__title">Tytuł CTA</h3>
                <div class="cta__text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis</p>
                </div>
                <div class="cta__text cta__text--small">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis</p>
                </div>
            </div>
            <div class="small-12 medium-6 cell">
                <div class="cta__actions">
                    <a href="#" class="cta__button">Button #1</a>
                    <a href="#" class="cta__button cta__button--red">Button #1</a>
                </div>
            </div>
        </div>
    </div>
</section>
```

```scss
// Przykładowy plik /scss/components/_cta.scss
.cta {
    background: red;
    &__title {
        font-size: 1.5rem;
        font-weight: bold;
    }
    &__text {
        font-size: 1.2rem;
        color: #333333;
        &--small {
            font-size: 0.8rem;
        }
    }
    &__actions {
        text-align: right;
    }
    &__button {
        @extend .button; // .button from foundation;
        &--red {
            background-color: red;
            &:hover {
                background-color: red;
            }
        }
    }
}
```

Dzięki takiemu kodowaniu, każdy moduł można wkleić w dowolne miejsce na stronie przy zachowaniu stylowania, ponieważ nie zależy ono od klasy jego rodziców, czy strony na której się znajduje. Dlatego też, dodając np. odstępy pomiędzy sekcjami, ustalamy je jako `padding` wewnątrz sekcji, a nie np. jako `margin-top` dla nagłówka sekcji następnej. Każda sekcja powinna wyglądać poprawnie sama.

### RWD
Każdy projekt (nawet, jeśli nie ma na to makiety) musi dobrze wyglądać na telefonach. W scss korzystamy z wbudowanych w foundation mixinów, przykładowo:
```scss
.section {
    &__title {
        font-size: 1.2rem; // Default - mobile
        @include breakpoint(medium){ // Medium up (domyślnie od 640px)
            font-size: 1.8rem;
        }
        @include breakpoint(large){ // Medium up (domyślnie od 1024px)
            font-size: 2rem;
        }
    }
}
```

### Elementy aktywne
Każdy element aktywny powinien zmieniać swój styl na hover (lekka zmiana kolory / tła / opacity itp.) z `transition`.

W przypadku, gdy na stronie mamy np. kafelki linkujące do produktów (zdjęcie + nagłówek), aktywne do kliknięcia mają być całe te element, a nie tylko np. treść.

![Przykład kafelków](assets/tile_hover.mov)

Na powyższym przykładzie cały kafelek jest jednym odnośnikiem `<a>` - gdyby w środku miał być np. przycisk, robimy go jako `<span>` z dodatkowym efektem `span:hover`

### Zdjęcia i tła
Wszędzie, gdzie to tylko możliwe, staramy się umieszczać zdjęcia przy pomocy tagu `<img>`. Jedynie tam, gdzie zdjęcie rzeczywiście jest tłem lub jest to konieczne korzystamy z `background-image`. W szablonie statycznym w miejscach, gdzie tła będą podmieniane z WP, ustawiamy to bezpośrednio w HTMLu:
```html
<div class="test-img" style="background-image:url('./assets/img/bg.jpg');"></div>
```
W ten sposób możliwa będzie podmiana pliku w szablonie PHP. Oczywiście inne atrybuty takie jak `background-size`, `background-position` itp. ustawiamy już w plikach scss.

Zobacz też: [Lazyload](#lazyload)

### Nagłówki
Stosujemy się do dobrych praktyk SEO. Na każdej stronie może i musi być dokładnie jeden nagłówek `<h1>`. Wszystkie nagłówki stylujemy przy pomocy klasy tak, aby po analizie SEO możliwa była zmiana (np. z h3 na h4) bez utraty stylu. Czyli:
```html
<section class="section">
    <h2 class="section__title">Tytuł 2</h2>
</section>
```
```scss
// TAK ROBIMY:
.section {
    &__title {
        // STYLE
    }
}
```
```scss
// TAK NIE ROBIMY:
.section {
    h2 {
        // STYLE
    }
}
```

### Kodowanie menu (w szablonie statycznym)
Kod HTML menu z założenia będzie się generować z WordPressa - dlatego w szablonach bazowych trzeba się trzymać struktury HTMLowej z Wordpressa, czyli:
```html
<ul class="menu main-menu">
	<li class="current-menu-item"><a href="index.html">Home</a></li>
	<li class="menu-item-has-children"> <!-- klasa menu-item-has-children dodaje strzałeczki na mobile -->
		<a href="#">Parent Menu Item</a>
		<div class="sub-menu"> <!-- klasa sub-menu ważna -->
			<li><a href="contact.html">Submenu Item #1</a></li>
			<li><a href="contact.html">Submenu Item #2</a></li>
		</div>
	</li>
	<li><a href="contact.html">Contact</a></li>
</ul>
```
Wszystkie style css i skryptu JS obsługujące zarówno menu na RWD (hamburger) jak i submenu są już przygotowane - wystarczy je modyfikować (`/scss/components/_nav.scss` oraz `/js/modules/menu.js`).

Style odpowiadające za menu zmieniamy w `/src/scss/components/_nav.scss`

### Kodowanie formularzy  (w szablonie statycznym)
Większość formularzy będzie generowana przez wtyczkę Contact Form 7 - dlatego w szablonach bazowych trzeba się trzymać odpowiedniej struktury HTMLowej, przykładowo:
```html
<form action="/#wpcf7-f854-o1" method="post" class="wpcf7-form init" novalidate="novalidate">
	<span class="wpcf7-form-control-wrap your-name wpcf7-form-control-wrap--type-text">
	    <input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="imię">
	</span>
	
	<span class="wpcf7-form-control-wrap your-email wpcf7-form-control-wrap--type-email">
	    <input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="e-mail">
	</span>
	
	<span class="wpcf7-form-control-wrap your-message">
	    <textarea name="your-message" cols="40" rows="2" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="wpisz treść wiadomości"></textarea>
	</span>
	
	<input type="submit" value="Wyślij" class="wpcf7-form-control wpcf7-submit form-button">
</form>
```
Oczywiście wszystkie bloki `<span>...</span>` możemy dowolnie wkładać np. w grid itp. __Nie__ zmieniamy natomiast niczego wewnątrz bloków `<span>`, ani ich klas - są całkowicie generowane przez WP.

## Wordpress

### ACF
Podczas tworzenia stron wszelkie dodatkowe treści wyświetlamy przy pomocy wtyczki [Advanced Custom Fields Pro](https://www.advancedcustomfields.com/resources/). Pola umieszczamy w WP bazując na szablonach strony (NIE na konkretnych stronach, czy ID)

Mamy też możliwość dodawania "globalnych" pól (np. z danymi w stopce). W szablonie przygotowana została zakładka "Opcje strony" do której możemy  dodać ACFy. Później w szablonie wyświetlamy wartości przy użyciu funkcji `get_translated_option()`.

Aby wyświetlić wartość z głównej zakładki "Opcje strony" używamy:
```php
<?php echo get_translated_option('tresc_stopka');?>
```

Możemy także tworzyć dodatkowe zakładki opcji.  Jest to przydatne, gdy chcemy dodać własne pola np. do archiwum wpisów (lub custom-post-type) Przykładowo, jeśli chcemy stworzyć stronę opcji dla wpisów, możemy użyć:
```php
add_acf_submenu_options('post-options', 'Opcje wpisów', 'edit.php');
// dla własnego typu wpisów ?edit.php?post_type=post-type-slug
```

Później możemy wyświetlać wartości pól przy użyciu

```php
<?php echo get_translated_option('nazwa_pola', 'post-options');?>
```

Używanie tych funkcji jest kompatybilne z polylangiem - dla każdego języka tworzą się dodatkowe wersje zakładek z opcjami.

Wyświetlając pola, __nigdy nie używamy zakodowanego na sztywno ID strony__. Jeśli jakaś treść ma się powtarzać na różnych stronach, korzystamy z zakładek stron opcji opisanych powyżej. Jeśli już musimy na jakiejś stronie wyświetlić np. wartość pola ze strony głównej, robimy to przy użyciu
```php
<?php the_field('nazwa_pola', get_option('page_on_front'));?>
```
Nie robimy tak: ~~`<?php the_field('nazwa_pola', 62);?>`~~

### Szablony stron
Stronę główną możemy zakodować przy użyciu `front-page.php` lub szablonu strony z `/templates`.

Wszelkiego rodzaju archiwa (produktów, newsów itp.) kodujemy jako archiwa typów treści (z definicją własnego custom-post-type).

Przykładowo - archiwum produktów to `archive-product.php` z globalnym `WP_Query`, a nie strona z szablonem i własnym `WP_Query` do wyświetlania wpisów.

Jeśli kilka stron ma taki sam lub bardzo podobny szablon, tworzymy jeden uniwersalny plik w `/templates`. Ewentualne różnice możemy kontrolować przy użyciu ACF. Jeśli nie ma takiej potrzeby, nie tworzymy osobnego szablonu dla każdej ze stron.

W razie, gdyby klient chciał tworzyć własne proste strony tekstowe (np. z regulaminem, polityką prywatności itp.) zadbajmy też, żeby `page.php` wyglądało dobrze.

### Zdjęcia
W wordpressie zdjęcia (tam, gdzie to możliwe) wyświetlamy przy użyciu 
```php
<?php echo wp_get_attachment_image($img_id, $img_size);?>
```
Pozwoli to wyświetlić atrybuty `alt` ustawione w wordpressie.

Tła ustawiamy następująco (zobacz też [Lazyload](#lazyload)):
```php
<?php $img = wp_get_attachment_image_src($img_id, $img_size);?>
<div class="test-img" style="background-image:url('<?php if($img) echo $img[0];?>');"></div>
```

### Breadcrumbs
Po włączeniu okruszków w zakładce _SEO_ / _Wyszukiwarki_ / _Okruszki_, w szablonie wyświetlamy je przy użyciu
```php
<?php if ( function_exists('yoast_breadcrumb') ) yoast_breadcrumb( '<nav aria-label="breadcrumbs" id="breadcrumbs">','</nav>' );?>
```

# Funkcjonalności dostępne w szablonie
## Fixed header
Dodanie do `body` klasy `fixed-nav` (w wersji WP wystarczy ustawić zmienną `has_fixed_nav` w `src/header.php` na `true`) ustawia nagłówek jako `fixed` i dodaje do `#site-wrap` `padding` tak, aby cała treść strony była widoczna i nie ucięta. Wysokość `padding` można zmienić w `src/scss/general/_general.scss`.

## Slider
W szablonie zainstalowana jest wtyczka [slick-slider](https://kenwheeler.github.io/slick/). Przykładowa implementacja slidera:
```html
<!-- src/partials/home/main-slider.php -->
<div class="home-slider slick-slider" id="home-slider">
	<div class="home-slider__slide"><p>Lorem Ipsum</p></div>
	<div class="home-slider__slide"><p>Lorem Ipsum</p></div>
	<div class="home-slider__slide"><p>Lorem Ipsum</p></div>
</div>
```
```javascript
// src/js/modules/sliders.js
$('#home-slider').slick({
	loop: true,
	dots: true,
});
```
## Lightbox
W szablonie zainstalowana jest wtyczka [fancybox](http://fancyapps.com/fancybox/3/). Wystarczy dla linków ze zdjęciami dodać atrybut `data-fancybox`:
```html
<a href="full-img.jpg" data-fancybox><img src="small-img.jpg"></a>
```
Jeśli chcemy utworzyć galerię (lub kilka galerii), nadajemy atrybutowi wartości
```html
<!-- Galeria -->
<a href="full-img_1.jpg" data-fancybox="galeria-1"><img src="small-img_1.jpg"></a>
<a href="full-img_2.jpg" data-fancybox="galeria-1"><img src="small-img_2.jpg"></a>

<!-- Inna galeria -->
<a href="full-img_3.jpg" data-fancybox="galeria-2"><img src="small-img_3.jpg"></a>
```
Fancybox uruchomi się automatycznie dla galerii osadzonych z wordpressa (selektor jQuery `$('.post-content .gallery')` - można zmienić w `/src/js/modules/wp-gallery.js`)

## Lazyload
W szablonie zainstalowana jest wtyczka [lazysizes](https://github.com/aFarkas/lazysizes). Została skonfigurowana, aby automatycznie działać z obrazkami w treściach wpisów oraz tymi wyświetlanymi przez `wp_get_attachment_image()`.

Aby ręcznie ustawić lazyload dla obrazka, należy użyć atrybutu `data-src` i klasy `lazyload`:
```html
<img data-src="img.jpg" class="lazyload">
```
Wtyczka wspiera też obrazki w tle (z atrybutem `data-bg`):
```html
<div data-bg="img.jpg" class="lazyload"></div>
```

## AJAX Loadmore
W szablonie skonfigurowane zostało doładowywanie nowych wpisów AJAXem. Działa wszędzie tam, gdzie używamy globalnego `WP_Query`, czyli w wszelkiego rodzaju archiwach wpisów, kategorii itp.

Przykładowy kod realizujący funkcję (demo w archiwum `index.php` szablonu bazowego WP):
```php
<?php global $wp_query;?>

<div class="grid-container">

	<div class="grid-x medium-up-3 grid-margin-x grid-margin-y" id="loop"> 
		<?php if ( have_posts() ):?>
			<?php while ( have_posts() ): the_post();?>
				<?php get_template_part('partials/post/content-loop');?>
			<?php endwhile;?>
		<?php endif; ?>
	</div>

	<?php if($wp_query->max_num_pages > 1):?>
		<div class="text-center">
			<a href="#" class="button" 
				data-loadmore 
				data-wrap="#loop" 
				data-template="partials/post/content-loop.php"
			>
				<span class="text"><?php a_e('Zobacz więcej');?></span>
				<span class="loading"><i class="material-icons">refresh</i></span>
			</a>
		</div>
	<?php endif;?>
	
</div>
```
Ważne elementy:
- link z atrybutem `data-loadmore`
- połączenie id kontenera z linkiem przy pomocy atrybutu `data-wrap`
- wyświetlanie wpisów przez `get_template_part()` i nazwa pliku w atrybucie `data-template`

## Lokalizacja (polylang)

Wszelkie teksty, które nie są wprowadzane z Wordpressa (np. treści przycisków w kafelkach w archiwum) wyświetlamy przy pomocy funkcji lokalizujących
```php
a__('Text') // Zwraca string
a_e('Text') // Wyświetla string ( a_e() jest tym samym co echo a__() )
```
Przykładowo:
```php
<a href="<?php the_permalink();?>"><?php a_e('Czytaj więcej');?></a>
```
Funkcje te automatycznie rejestrują stringi w bazie polylang, więc później będą od razu dostępne do tłumaczenia.